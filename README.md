# GIT 101


## System Requirements

- [git][git] v2 or greater

All of these must be available in your `PATH`. To verify things are set up
properly, you can run this:

```shellell
git --version
```

## Workshop workflow
The workflow of this workshop is fairly simple and based on Make It Stick methodologies:

1. Learn a few concepts
2. Apply the concepts via hands-on (exercises)
3. Show your progress and ask questions


## Terminology
| Term       | Description                                                                                                     |
| ---------- | --------------------------------------------------------------------------------------------------------------- |
| Repository | A repository is a collection of various files of a Project                                                      |
| Branch     | A branch is separated line where changes can be made without changing the main line                             |
| Remote     | A remote repository is stored on a code hosting service where members can exchange their changes to the project |


### Outline

1. [Task 0][task-00]:  What is `git`
2. [Task 1][task-01]: `git init` or `git clone`
3. [Task 2][task-02]: `git status`
4. [Task 3][task-03]: `git branch`
5. [Task 4][task-04]: `git add`
7. [Task 5][task-05]: `git commit`
8. [Task 6][task-06]: `git push`
9. [Task 7][task-07]: `git pull`

<!-- prettier-ignore-start -->
[git]: https://git-scm.com/
[task-00]: ./00.md
[task-01]: ./01.md
[task-02]: ./02.md
[task-03]: ./03.md
[task-04]: ./04.md
[task-05]: ./05.md
[task-06]: ./06.md
[task-07]: ./07.md
<!-- prettier-ignore-end -->