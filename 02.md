# Git status
> View the current status of your project

When running `git status` in the command line, it will print some basic information such as which files have been recently modified.

After you have created changes to a document you should check the status of your project to see if your changes are correct.


##  💪🏻 Exercise 3: 
**Goal**: We are going to check our project status.

1. Open up a command prompt/ terminal
2. Navigate to the recent created project.
3. Type in `git status`, what do you see?
```shell
$ git status
On branch master
nothing to commit, working tree clean
```

4. Let's create a file called `<YOUR_NAME>.md`.
5. Type in `git status`, what do you see now?
```shell
$ git status
On branch master
Untracked files:
  (use "git add <file>..." to include in what will be committed)
        jeffrey_bosch.md
nothing added to commit but untracked files present (use "git add" to track)
```

**💯 Review time 💯**
Let's see somebody's screen


**Ready for the next part click 👉🏻 [here][task-03]**


[task-03]: ./03.md

